import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import store from '../store';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    const auth = store.state.account.isAuthorized;
    let keys = [to.meta.auth, auth]; // берем у роутеров meta ключи и сверяем со стором, был ли пользователь авторизован
    to.meta.auth ? keys.every(key => key) ? next() : next('/login') : next();
});

export default router;
