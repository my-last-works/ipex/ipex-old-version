export default [
    {
        path: '/',
        name: 'home',
        component: () => import('pages/index')
    },
    {
        path: '/ois',
        name: 'ois',
        component: () => import('pages/Ois'),
        meta: {
            auth: false // layout: 'detail' разобраться потом
        }
    },
    {
        path: '/sell-ois',
        name: 'sell-ois',
        component: () => import('pages/SellOis'),
        meta: {
            auth: false
        }
    },
    {
        path: '/ois/upload',
        name: 'oisUpload',
        component: () => import('pages/Ois/Upload'),
        meta: {
            layout: 'upload'
        }
    },
    {
        path: '*',
        component: () => import('pages/NotFoundPage')
    }
];
