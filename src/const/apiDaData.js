const API_URL = process.env.VUE_APP_API_PROJECT;
export const DA_DATA_TOPICS = API_URL + 'api/catalog/autocomplete/media/topics';
export const DA_DATA_MOODS = API_URL + 'api/catalog/autocomplete/media/moods';
export const DA_DATA_CATEGORIES = API_URL + 'api/catalog/autocomplete/media/categories';
export const DA_DATA_TAGS = API_URL + 'api/catalog/autocomplete/media/tags';
