const DEV_URL = 'https://www.dev.ipex.global/';
const API_URL = process.env.VUE_APP_API_PROJECT;
const API_URL_ID = process.env.VUE_APP_API_PROJECT_ID;

export const API_TOKEN = API_URL + 'api/token';
export const API_SUGGEST_EMAIL = DEV_URL + 'api/suggest/email';
export const API_PROFILE = API_URL + 'api/profile';
export const API_LOGOUT = API_URL_ID + 'logout';
export const API_BOOKMARKS_COUNT = API_URL + 'api/bookmarks/count';
export const API_IP_CANDIDATES = API_URL + 'api/ipcandidates';
