export default {
    namespaced: true,
    state: {
        apiErrors: {}
    },
    getters: {

    },
    mutations: {
        CLEAR_API_ERRORS(state) {
            state.apiErrors = {};
        },
        ADD_API_ERROR(state, error) {
            if (error.toString() === '[object Object]') {
                state.apiErrors = Object.assign({}, state.apiErrors, error);
                return;
            }
            return console.warn("'ADD_API_ERROR' принимает только объекты!");
        }
    },
    actions: {
        clearApiErrors({ commit }) {
            commit('CLEAR_API_ERRORS');
        },
        addApiError({ commit }, error) {
            commit('ADD_API_ERROR', error);
        }
    }
};
