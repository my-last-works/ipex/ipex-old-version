import localization from './localization';
import api from './api';
import information from './information';
import account from './account';
import face from './interface';
import ois from './ois';

export default {
    localization,
    api,
    information,
    account,
    interface: face,
    ois
};
