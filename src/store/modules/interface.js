export default {
    namespaced: true,
    state: {
        loader: false
    },
    getters: {
        $loader(state) {
            return state.loader;
        }
    },
    mutations: {
        SET_LOADER(state, boolean) {
            state.loader = !!boolean;
        }
    },
    actions: {
        setLoader({ commit }, boolean) {
            console.log('setLoader');
            commit('SET_LOADER', boolean);
        }
    }
};
