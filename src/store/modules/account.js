import { API_TOKEN, API_PROFILE, API_LOGOUT, API_BOOKMARKS_COUNT } from 'const/api';
import Vue from 'vue';

export default {
    namespaced: true,
    state: {
        isAuthorized: false,
        token: '',
        user: {},
        favorites: null
    },
    getters: {
        getUserEmail(state) {
            return state.user.email || null;
        }
    },
    mutations: {
        LOGOUT(state) {
            state.isAuthorized = false;
        },
        LOGIN(state) {
            state.isAuthorized = true;
        },
        SET_TOKEN(state, token) {
            state.token = token;
        },
        CHANGE_AUTHORIZED(state) {
            state.isAuthorized = !state.isAuthorized;
        },
        SET_USER_INFORMATION(state, payload) {
            state.user = payload;
        },
        CLEAR_USER_INFORMATION(state) {
            state.user = {};
        },
        LOGIN_USER(state, data) {
            state.user = data;
            state.isAuthorized = true;
        },
        LOGOUT_USER(state) {
            state.user = {};
            state.isAuthorized = false;
            state.favorites = null;
        },
        SET_FAVORITES(state, favorites) {
            state.favorites = favorites;
        }
    },
    actions: {
        async login({ commit, dispatch }) {
            await dispatch('getAndSetAccountInfo');
            dispatch('getAndSetFavorites');
            commit('LOGIN');
        },
        generateToken({ commit, dispatch }) {
            return axios.get(API_TOKEN, { withCredentials: true })
                .then(response => {
                    if (response.data) {
                        commit('SET_TOKEN', response.data);
                        dispatch('tokenUpdateInterval');
                    }
                })
                .catch(error => {
                    console.error('При получении token произошла ошибка!', error);
                });
        },
        tokenUpdateInterval({ dispatch }) {
            if (window.tokenTimeout) clearTimeout(window.tokenTimeout);
            window.tokenTimeout = setTimeout(async () => {
                await dispatch('generateToken');
            }, 60000 * 9);
        },
        checkAuthorized({ dispatch }) {
            return axios.get(API_PROFILE, { withCredentials: true })
                .then(response => {
                    dispatch('changeAuthorized');
                    dispatch('setUserInformation', response.data);
                });
        },
        getAndSetAccountInfo({ dispatch }) {
            return axios.get(API_PROFILE, { withCredentials: true })
                .then(response => {
                    dispatch('setUserInformation', response.data);
                });
        },
        getAndSetFavorites({ dispatch }) {
            Vue.prototype.$http.get(API_BOOKMARKS_COUNT, { startRequest: false, finallyRequest: false })
                .then(response => dispatch('setFavorites', response.data));
        },
        changeAuthorized({ commit }) {
            commit('CHANGE_AUTHORIZED');
        },
        setUserInformation({ commit }, payload) {
            commit('SET_USER_INFORMATION', payload);
        },
        loginUser({ commit, dispatch }, data) {
            dispatch('getAndSetFavorites');
            commit('LOGIN_USER', data);
        },
        logout({ commit, dispatch }) {
            return Vue.prototype.$http.get(API_LOGOUT)
                .then(async () => {
                    await commit('LOGOUT_USER');
                    dispatch('generateToken');
                });
        },
        logoutUser({ commit }) {
            commit('LOGOUT_USER');
        },
        setFavorites({ commit }, favorites) {
            commit('SET_FAVORITES', favorites);
        }
    }
};
