export default {
    data() {
        return {
            focusElement: null
        };
    },
    methods: {
        clearFocusElement() {
            this.focusElement = null;
        },
        setFocusElement(e) {
            this.focusElement = e.target.name;
        }
    }
};
