import Vue from 'vue';
import toastr from 'toastr';
import options from './config/toastr';

toastr.options = options;
Vue.use(toastr);
