import Vue from 'vue';
import store from '@/store';
import axios from 'axios';
import toastr from 'toastr';
import router from 'src/router';

class Http extends axios {
    static requests = {
        post: {
            headers: {
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
            withCredentials: true
        },
        get: {
            withCredentials: true
        }
    };

    static install(Vue, options) {
        Vue.prototype.$http = this;
    }

    static parseError(error) {
        const { response } = error;
        if (response) {
            const { status, data } = response;
            if (status && this[`error${status}`]) {
                return this[`error${status}`]();
            }
            return store.dispatch('api/addApiError', { [router.currentRoute.name]: data });
        }
        return this.notFoundResponse();
    }

    static error404() {
        toastr.error('Произошел сбой! Попробуйте позже.', '404 Ошибка!');
    }

    static error419() {
        toastr.error('Произошел сбой! Попробуйте позже.', '419 Ошибка!');
    }

    static notFoundResponse() {
        toastr.error('Произошел сбой! Попробуйте позже.', 'Ошибка!');
    }

    static startRequest() {
        store.dispatch('interface/setLoader', true);
        store.dispatch('api/clearApiErrors');
    }

    static finallyRequest() {
        store.dispatch('interface/setLoader', false);
    }

    static get(...params) {
        const token = store.state.account.token;
        let [url, options = {}] = params;
        let { data, withCredentials, sendToken = false, startRequest = true, finallyRequest = true } = options;

        if (startRequest) this.startRequest();

        if (!withCredentials) options = Object.assign(options, this.requests.get);

        if (sendToken) {
            if (!data) options['data'] = {};
            options.data = Object.assign(options.data, {
                _token: token
            });
            delete options.token;
        }

        return super.get(url, options)
            .catch(error => {
                this.parseError(error, url);
                throw new Error(error) && error;
            })
            .finally(() => {
                if (finallyRequest) this.finallyRequest();
            });
    }

    static post(...params) {
        const token = store.state.account.token;
        let [url, data = {}, options = {}] = params;
        let { headers, withCredentials, startRequest = true, finallyRequest = true } = options;

        if (startRequest) this.startRequest();

        if (!withCredentials) {
            options = Object.assign({}, {
                ...options,
                withCredentials: this.requests.post.withCredentials
            });
        }

        if (!headers) options = Object.assign(options, { headers: this.requests.post.headers });

        if (data instanceof FormData) {
            data.append('_token', token);
        } else {
            data = Object.assign(data, {
                _token: token
            });
        }

        return super.post(url, data, options)
            .catch(error => {
                this.parseError(error);
                throw new Error(error) && error;
            })
            .finally(() => {
                if (finallyRequest) this.finallyRequest();
            });
    }
}

Vue.use(Http);
