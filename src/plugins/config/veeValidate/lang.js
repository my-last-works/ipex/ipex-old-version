import ru from 'vee-validate/dist/locale/ru';
import en from 'vee-validate/dist/locale/en';
import dayjs from 'dayjs';

export default {
    ru: {
        attributes: {
            EMAIL: 'E-mail',
            PASSWORD: ' ',
            additionalEmail: ' ',
            ...ru.attributes
        },
        messages: {
            phoneOrEmail: (...params) => {
                const [field, , { key }] = params;
                return key === 'phone'
                    ? 'Введите корректный номер телефона.'
                    : `Поле ${field} должно быть действительным адресом электронной почты.`;
            },
            password: 'Пароль должен быть не менее 6 символов, содержать латинские строчные и заглавные буквы, и цифры.',
            repassword: 'Подтверждение пароля не совпадает.',
            phoneCode: 'Поле должно быть числовым и точно содержать 6 цифры.',
            seriesPassport: 'Серия паспорта должна состоять из 10 цифр',
            bDay: 'Ваш возраст должен быть 18+',
            datePassport: `Паспорт мог быть выдан с 01.10.1997 года и по ${dayjs(new Date()).format('DD.MM.YYYY')}`,
            codePassport: 'Код подразделения должен состоять из 6 цифр',
            additionalEmail: 'Поле должно быть действительным адресом электронной почты.',
            requireHideName: 'Поле обязательно для заполнения',
            ...ru.messages
        }
    },
    en: {
        attributes: {
            EMAIL: '(EN) E-mail',
            PASSWORD: ' ',
            additionalEmail: ' ',
            ...en.attributes
        },
        messages: {
            phoneOrEmail: (...params) => {
                const [field, , { key }] = params;
                return key === 'phone'
                    ? 'Введите корректный номер телефона.'
                    : `(EN) Поле ${field} должно быть действительным адресом электронной почты.`;
            },
            password: '(EN) Пароль должен быть не менее 6 символов, содержать латинские строчные и заглавные буквы, и цифры.',
            repassword: '(EN) Подтверждение пароля не совпадает.',
            phoneCode: '(EN) Поле должно быть числовым и точно содержать 6 цифры.',
            seriesPassport: '(EN) Серия паспорта должна состоять из 10 цифр',
            bDay: 'Ваш возраст должен быть 18+',
            datePassport: `(EN) Паспорт мог быть выдан с 01.10.1997 года и по ${dayjs(new Date()).format('DD.MM.YYYY')}`,
            codePassport: '(EN) Код подразделения должен состоять из 6 цифр',
            additionalEmail: '(EN) Поле должно быть действительным адресом электронной почты.',
            requireHideName: '(EN) Поле обязательно для заполнения',
            ...en.messages
        }
    }
};
