import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './plugins/autoloadBaseComponents';
import './plugins/autoloadTemplates';
import './plugins/veeValidate';
import './plugins/vuebar';
import './plugins/vueMask';
import './plugins/http';
import './plugins/toastr';
import './plugins/vueScrollTo';
import i18n from './plugins/i18n';
import dayjs from 'dayjs';

Vue.prototype.$dayjs = dayjs;

Vue.config.productionTip = false;

// TODO: использовать прототип и константы
axios.get('https://id.test.ipex.global/api/profile', { withCredentials: true })
    .then(({ data }) => store.dispatch('account/loginUser', data))
    .catch(() => store.dispatch('account/logoutUser'))
    .finally(() => VUE());

// ! Нет нужды в функции
function VUE() {
    const app = new Vue({
        el: '#app',
        components: { App },
        template: '<App/>',
        router,
        store,
        i18n
    });

    /**
     * При работе на localhost добавим APP в глобальную видимость console браузера
     */

    if (process.env.NODE_ENV === 'development') window.APP = app;
}
