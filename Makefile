#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/sh

REGISTRY_HOST = gitlab.ixmsk.d-net.pro:4567
REGISTRY_PATH = ipex/ipex-front/
IMAGES_PREFIX := $(shell basename $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))

PUBLISH_TAGS = latest
PULL_TAG = latest

# Important: Local images naming should be in docker-compose naming style

F_SOURCES_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)f_sources
F_SOURCES_IMAGE_LOCAL_TAG = $(IMAGES_PREFIX)_f_sources
F_SOURCES_IMAGE_DOCKERFILE = ./.docker/f-sources/Dockerfile
F_SOURCES_IMAGE_CONTEXT = ./.

F_NGINX_IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)f_nginx
F_NGINX_IMAGE_LOCAL_TAG = $(IMAGES_PREFIX)_f_nginx
F_NGINX_IMAGE_DOCKERFILE = ./.docker/f-nginx/Dockerfile
F_NGINX_IMAGE_CONTEXT = ./.

F_NODE_CONTAINER_NAME := f-node
F_NGINX_CONTAINER_NAME := f-nginx

docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose_bin := $(shell command -v docker-compose 2> /dev/null)
DOCKER_ARGS = run -v $$(pwd):/var/www/html -w /var/www/html --rm -d alpine

all_images = $(F_SOURCES_IMAGE) \
             $(F_SOURCES_IMAGE_LOCAL_TAG) \
             $(F_NGINX_IMAGE) \
             $(F_NGINX_IMAGE_LOCAL_TAG)

ifeq "$(REGISTRY_HOST)" "gitlab.ixmsk.d-net.pro:4567"
	docker_login_hint ?= "\n\
	**************************************************************************************************\n\
	* Make your own auth token here: <https://gitlab.ixmsk.d-net.pro/profile/personal_access_tokens> *\n\
	**************************************************************************************************\n"
endif

.PHONY : help pull build push login test clean \
         sources-pull sources sources-push\
         nginx-pull nginx nginx-push\
         up down restart shell install
.DEFAULT_GOAL := help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n  Allowed for overriding next properties:\n\n\
	    PULL_TAG - Tag for pulling images before building own\n\
	              ('latest' by default)\n\
	    PUBLISH_TAGS - Tags list for building and pushing into remote registry\n\
	                   (delimiter - single space, 'latest' by default)\n\n\
	  Usage example:\n\
	    make PULL_TAG='v1.2.3' PUBLISH_TAGS='latest v1.2.3 test-tag' app-push"


# --- [ Sources ] -----------------------------------------------------------------------------------------------------

sources-pull: ## Sources - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(F_SOURCES_IMAGE):$(PULL_TAG)"

sources: ## Sources - build Docker image locally
	$(docker_bin) build \
	  --tag "$(F_SOURCES_IMAGE_LOCAL_TAG)" \
	  -f $(F_SOURCES_IMAGE_DOCKERFILE) $(F_SOURCES_IMAGE_CONTEXT)

sources-push: ## Sources - tag and push Docker image into remote registry
	$(docker_bin) build \
	  $(foreach tag_name,$(PUBLISH_TAGS),--tag "$(F_SOURCES_IMAGE):$(tag_name)") \
	  -f $(F_SOURCES_IMAGE_DOCKERFILE) $(F_SOURCES_IMAGE_CONTEXT);
	$(foreach tag_name,$(PUBLISH_TAGS),$(docker_bin) push "$(F_SOURCES_IMAGE):$(tag_name)";)

# --- [ Nginx ] -------------------------------------------------------------------------------------------------------

nginx-pull: ## Nginx - pull latest Docker image (from remote registry)
	-$(docker_bin) pull "$(F_NGINX_IMAGE):$(PULL_TAG)"

nginx: nginx-pull ## Nginx - build Docker image locally
	$(docker_bin) build \
	  --cache-from "$(F_NGINX_IMAGE):$(PULL_TAG)" \
	  --tag "$(F_NGINX_IMAGE_LOCAL_TAG)" \
	  -f $(F_NGINX_IMAGE_DOCKERFILE) $(F_NGINX_IMAGE_CONTEXT)

nginx-push: nginx-pull ## Nginx - tag and push Docker image into remote registry
	$(docker_bin) build \
	  --cache-from "$(F_NGINX_IMAGE):$(PULL_TAG)" \
	  $(foreach tag_name,$(PUBLISH_TAGS),--tag "$(F_NGINX_IMAGE):$(tag_name)") \
	  -f $(F_NGINX_IMAGE_DOCKERFILE) $(F_NGINX_IMAGE_CONTEXT);
	$(foreach tag_name,$(PUBLISH_TAGS),$(docker_bin) push "$(F_NGINX_IMAGE):$(tag_name)";)

# ---------------------------------------------------------------------------------------------------------------------

pull: nginx-pull sources-pull ## Pull all Docker images (from remote registry)

build: sources nginx ## Build all Docker images

push: sources-push nginx-push ## Tag and push all Docker images into remote registry

login: ## Log in to a remote Docker registry
	@echo $(docker_login_hint)
	$(docker_bin) login $(REGISTRY_HOST)

clean: ## Remove images from local registry
	-$(docker_compose_bin) down -v
	$(foreach image,$(all_images),$(docker_bin) rmi -f $(image);)

# --- [ Development tasks ] -------------------------------------------------------------------------------------------

---------------: ## ---------------

up: ## Start all containers (in background) for development
	$(docker_bin) network create --driver bridge ipex_network 2>/dev/null || true
	$(docker_compose_bin) pull && $(docker_compose_bin) up -d

down: ## Stop all started for development containers
	$(docker_compose_bin) down

restart: up ## Restart all started for development containers
	$(docker_compose_bin) restart

shell: up ## Start shell into application container
	$(docker_compose_bin) exec "$(F_NGINX_CONTAINER_NAME)" /bin/bash

install: up ## Install application dependencies into application container
	$(docker_compose_bin) run --rm "$(F_NODE_CONTAINER_NAME)" sh -c 'npm install'
	$(docker_compose_bin) run --rm "$(F_NODE_CONTAINER_NAME)" sh -c 'npm run build'

init: install ## Make full application initialization (install, seed, build assets, etc)

test: up ## Execute application tests
